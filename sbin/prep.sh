#!/bin/sh -x
acp(){
  cp $1 $1.ap
  cp $1 $1.router
}

acp network
acp wireless
acp dhcp
acp firewall

sed -ie'/config.*lan/,100d' network.router
cat >> network.router << EOF0

config interface 'wan'
        option ifname 'eth0'
        option proto 'dhcp'

config interface 'wifi'
        option proto 'static'
        option ipaddr '192.168.2.1'
        option netmask '255.255.255.0'

EOF0

sed -ie '/REMOVE THIS LINE/,12d' wireless.router
sed -ie 's/option network.*lan/option network wifi/' wireless.router
sed -ie 's/option ssid.*/option ssid VeereshKhanorkar/' wireless.router
sed -ie 's/option encryption.*/option encryption psk2/' wireless.router
sed -i '$d' wireless.router
cat >> wireless.router << EOF1
        option key v333r3sh5751
EOF1

cat >> dhcp.router << EOF2

config dhcp wifi
        option interface        wifi
        option start            100
        option limit            150
        option leasetime        12h

EOF2

cat >> firewall.router << EOF3

config zone
        option name             wifi
        option input            ACCEPT
        option output           ACCEPT
        option forward          REJECT

config forwarding
        option src              wifi
        option dest             wan

EOF3
